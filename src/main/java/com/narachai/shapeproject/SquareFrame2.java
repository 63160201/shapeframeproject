/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.narachai.shapeproject;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author ASUS
 */
public class SquareFrame2 extends JFrame{
    JLabel lblSide;
    JTextField txtSide;
    JButton btnClaculate;
    JLabel lblResult;

    public SquareFrame2() {
        super("Square");
        this.setSize(300, 300);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(null);

        lblSide = new JLabel("side:", JLabel.TRAILING);
        lblSide.setSize(30, 20);
        lblSide.setLocation(5, 5);
        lblSide.setBackground(Color.WHITE);
        lblSide.setOpaque(true);
        this.add(lblSide);
        
        txtSide = new JTextField();
        txtSide.setSize(50, 20);
        txtSide.setLocation(40, 5);
        this.add(txtSide);
        
        btnClaculate = new JButton("Claculate");
        btnClaculate.setSize(100, 20);
        btnClaculate.setLocation(100, 5);
        this.add(btnClaculate);
        
        lblResult = new JLabel("Square side=??? area=??? perimeter=???");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(300, 50);
        lblResult.setLocation(0, 50);
        lblResult.setBackground(Color.MAGENTA);
        lblResult.setOpaque(true);
        this.add(lblResult);
        
        btnClaculate.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                try{
                String strSide = txtSide.getText();
                double side = Double.parseDouble(strSide);
                Square square = new Square(side);
                lblResult.setText("Square side = "+String.format("%.2f",square.getSide())
                        +" area = " + String.format("%.2f",square.calArea()) 
                        + " perimeter = " + String.format("%.2f",square.calPerimeter()));
                }catch(Exception ex){
                    JOptionPane.showMessageDialog(SquareFrame2.this, "Error : Please input number",
                            "Error", JOptionPane.ERROR_MESSAGE);
                    txtSide.setText("");
                    txtSide.requestFocus();

                }
            }    
        });
    }

    public static void main(String[] args) {
        SquareFrame2 frame = new SquareFrame2();
        frame.setVisible(true);
    }
}
